require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
    if arr.length > 0
        arr.inject(:+)
    else
        0
    end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    if string.include?(substring)
      next
    else
      return false
    end
  end
  true
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.chars.select { |lett| string.count(lett) > 1 && lett != " "}.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  a = string.split.sort_by! {|word| word.length}
  a[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = %w{ a b c d e f g h i j k l m n o p q r s t u v w x y z}
  alphabet.select {|lett| lett if !string.include?(lett)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonder_arr = []
  songs.uniq.each do |song|
    wonder_arr << song if no_repeats?(song, songs)
  end
  wonder_arr
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    if song_name == song
      if song == songs[idx + 1]
        return false
      end
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  punctuation = %w{! ? . ,}
  index_arr = []
  str_no_punct = string.chars.reject {|lett| punctuation.include?(lett)}.join
  words_arr = str_no_punct.split 
  words_arr.each do |word|
    index_arr << c_distance(word)
  end
  idx = index_arr.index {|el| el == index_arr.min}
  words_arr[idx]
end

def c_distance(word)
  if !word.include?("c")
    return 80
  else
  word.chars.reverse.index("c") + 1
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  new_arr = []
  arr.each_with_index do |num, idx|
    if num == arr[idx + 1] && num != arr[idx - 1]
      idx_1 = idx
      count = 0
      until num != arr[idx + count]
        idx_2 = idx + count
        count += 1
      end
      new_arr << [idx_1, idx_2]
    end
  end
  new_arr
end
